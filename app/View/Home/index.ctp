<!--Hero-->
<div class="section no-pad-bot" id="hero">
    <div class="container">
        <br><br>
      <h3 class="header center white-text">Creando los juguetes del mañana con la tecnolgia del futuro</h3>
     <!--  <div class="row center">
        <h5 class="header col s12 light">A modern responsive front-end framework based on Material Design</h5>
      </div> -->
      <div class="row center">
        <a href="#contact" id="download-button" class="btn-large waves-effect waves-light blue">Iniciar</a>
      </div>
      <!-- <div class="row center">
        <a href="#contact" id="download-button" class="btn-large waves-effect waves-light white orange-text">Contacto</a>
      </div> -->
      <br><br>
    </div>
</div>
<!--Intro and service-->
<div id="intro" class="section scrollspy">
    <div class="container">
        <div class="row">
            <div  class="col s12">
                <h2 class="center header text_h2 orange-text"> ¿Quienes somos?</h2>
            </div>

            <div  class="col s12 m3 l3">
                <div class="center promo promo-example">
                    <i class="material-icons">flash_on</i>
                    <p class="light center">Los sitios se realizan con una buena estructura de la información, para más accesibilidad.</p>
                </div>
            </div>
            <div class="col s12 m3 l3">
                <div class="center promo promo-example">
                    <i class="material-icons">trending_up</i>
                    <p class="light center">Sabemos la importancia del posicionamiento, por eso las paginas webs son optimizadas, para que los buscadores indexen fácilmente.</p>
                </div>
            </div>
            <div class="col s12 m3 l3">
                <div class="center promo promo-example">
                    <i class="material-icons">assignment_ind</i>
                    <p class="light center">Cada web es analizada y oriendata, pensando en el mercado al cual va dirigido, personalizando a la imagen corporativa de cada cliente.</p>
                </div>
            </div>
            <div class="col s12 m3 l3">
                <div class="center promo promo-example">
                    <i class="material-icons">language</i>
                    <p class="light center">Estamos al pendiente de nuevas tecnologías para ofrecer el mejor servicio a los usuarios.</p>
                </div>
            </div>
        </div>
    </div>
</div>