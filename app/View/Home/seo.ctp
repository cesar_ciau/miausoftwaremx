<div class="section">
  <div class="row">
    <div class="col s12 ">
      <h4>¿Quienes Somos?</h4>
      <p>Somos un grupo de desarrolladores que nos apaciona la
        tecnologia, por lo cual decidimos compartir nuestros conocimientos con
        el mundo, en esta pagina encontraras algunas aplicaciones que te podrian
         ser utilez, las puedes descargar y probar bajo tu propio riesgo y
        criterio.
      </p>
      <p>En esta pagina encontraras aplicaciones gratuitas y de paga, si necesitas cotizacion de un proyecto o modificar alguno gratuito
         para tu empresa o negocio por favor contactanos para una cotizacion.</p>
    </div>
  </div>
  <div class="section">
    <h2 class="header blue-grey-text">App Store</h2>
    <div class="row">
        <div class="col s12 m4">
            <div class="card card-avatar center">
                <div class="waves-effect waves-block waves-light">
                    <span class="blue-grey-text text-darken-3 tiniest opacity-04">Aplicaciones Web</span>
                    <h5 class="center price-table-title blue-grey-text text-darken-3">Web</h5>
                </div>
                <div class="card-content" style="padding:5px">
                    <ul class="collection">
                      <li class="collection-item">HTML5</li>
                      <li class="collection-item">CSS3 y Material Design</li>
                      <li class="collection-item">JS</li>
                      <li class="collection-item">SEO</li>
                    </ul>
                    <h2 class="big text-darken-5">GRATIS</h2>
                    <a href="#%21" class="white-text btn pink darken-1">Ver Apps Web</a>
                    <br>
                </div>
            </div>
        </div>
        <div class="col s12 m4">
            <div class="center card card-avatar">
                <div class="waves-effect waves-block waves-light">
                    <span class="blue-grey-text text-darken-3 tiniest opacity-04">Android , Iphone</span>
                    <h5 class="price-table-title blue-grey-text text-darken-3">Movil</h5>
                </div>
                <div class="card-content" style="padding:5px">
                    <ul class="collection">
                      <li class="collection-item">Aplicaciones Veloces</li>
                      <li class="collection-item">Material Design</li>
                      <li class="collection-item">Email Support</li>
                      <li class="collection-item">Multiplataforma</li>
                    </ul>
                    <h2 class="big text-darken-5">GRATIS</h2>
                    <a href="#%21" class="white-text btn pink darken-1">Ver Apps Moviles</a>
                    <br>
                </div>
            </div>
        </div>
        <div class="col s12 m4">
            <div class="center card card-avatar">
                <div class="waves-effect waves-block waves-light">
                    <span class="blue-grey-text text-darken-3 tiniest opacity-04">Linux, Windows, Mac</span>
                    <h5 class="price-table-title blue-grey-text text-darken-3">Escritorio</h5>
                </div>
                <div class="card-content" style="padding:5px">
                    <ul class="collection">
                      <li class="collection-item">Backup Online/Offline</li>
                      <li class="collection-item">Ejecuta en un click</li>
                      <li class="collection-item">Email Support</li>
                      <li class="collection-item">Multiplataforma</li>
                    </ul>
                    <h2 class="big text-darken-5">GRATIS</h2>
                    <a href="#%21" class="white-text btn pink darken-1">Ver Aplicaciones</a>
                    <br>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>