<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout;?>:
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('materialize.min');
		echo $this->Html->css('custom');
		echo $this->Html->script('dontgo');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div class="navbar">
		<nav id ="menu-nav" role="navigation">
			<div class="nav-wrapper container">
				<a id="logo-container" href="#" class="brand-logo">Miausoftware</a>
				<ul class="right hide-on-med-and-down">
					<li><a href="http://miausoftware.mx/">Desarrollo</a></li>
					<li><a href="#">SEO</a></li>
					<li><a href="#">Marketing</a></li>
					<li><a href="#">Diseño Digital</a></li>
				</ul>
				<ul id="nav-mobile" class="side-nav">
					<li><a href="#">Navbar Link</a></li>
				</ul>
				<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
			</div>
		</nav>
	</div>
<?php echo $this->Flash->render(); ?>
<?php echo $this->fetch('content'); ?>
</body>
</html>
