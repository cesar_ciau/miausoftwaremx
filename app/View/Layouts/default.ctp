<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout;?>:
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('materialize.min');
		echo $this->Html->script('dontgo');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<header>
		<nav class="light-blue lighten-1" role="navigation">
			<div class="nav-wrapper container">
				<a id="logo-container" href="#" class="brand-logo">Miausoftware</a>
				<ul class="right hide-on-med-and-down">
					<li><a href="http://miausoftware.mx/">Desarrollo</a></li>
					<li><a href="#">SEO</a></li>
					<li><a href="#">Marketing</a></li>
					<li><a href="#">Diseño Digital</a></li>
				</ul>
				<ul id="nav-mobile" class="side-nav">
					<li><a href="#">Navbar Link</a></li>
				</ul>
				<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
			</div>
		</nav>
	</header>
	<div class="container">
		<div id="content">
			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
	</div>
	<footer id="contact" class="page-footer blue-grey scrollspy">
	    <div class="container">
	        <div class="row">
	            <div class="col l6 s12">
	                <form class="col s12" action="contact.php" method="post">
	                    <div class="row">
	                        <div class="input-field col s6">
	                            <i class="mdi-action-account-circle prefix white-text"></i>
	                            <input id="icon_prefix" name="name" class="validate white-text" type="text">
	                            <label for="icon_prefix" class="white-text">Nombre</label>
	                        </div>
	                        <div class="input-field col s6">
	                            <i class="mdi-communication-email prefix white-text"></i>
	                            <input id="icon_email" name="email" class="validate white-text" type="email">
	                            <label for="icon_email" class="white-text">Correo Electronico</label>
	                        </div>
	                        <div class="input-field col s12">
	                            <i class="mdi-editor-mode-edit prefix white-text"></i>
	                            <textarea id="icon_prefix2" name="message" class="materialize-textarea white-text"></textarea>
	                            <label for="icon_prefix2" class="white-text">Cotiza tu propio proyecto</label>
	                        </div>
	                        <div class="col offset-s7 s5">
	                            <button class="btn waves-effect waves-light red darken-1" type="submit">Enviar
	                                <i class="mdi-content-send right white-text"></i>
	                            </button>
	                        </div>
	                    </div>
	                </form>
	            </div>
	            <div class="col l4 s12">
	              <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
	                <li class="">
	                  <a href="tel:+5215512251508" class="">
	                    <div class="collapsible-header"><i class="material-icons">phone</i>DF, Jose Hasbun</div>
	                    </a><div class="collapsible-body" style="display: none;"><a href="tel:+5215512251508" class="">
	                  </a>
	                  </div>
	                </li>
	                <li class="">
	                  <a href="tel:+5219983142762">
	                    <div class="collapsible-header"><i class="material-icons">phone</i>Cancun, Cesar Ciau</div>
	                    </a><div class="collapsible-body" style="display: none;"><a href="tel:+5219983142762">
	                  </a>
	                  </div>
	                </li>
	                <li class="">
	                  <div class="collapsible-header"><i class="material-icons">thumb_up</i>Social</div>
	                  <div class="collapsible-body center" style="display: none;">
	                    <ul>
	                      <li>
	                        <a class="white-text" href="https://www.facebook.com/Miau-Software-458800494296088/">
	                          <i class="small fa fa-facebook-square white-text"></i> Facebook
	                        </a>
	                      </li>
	                    </ul>
	                  </div>
	                </li>
	              </ul>
	            </div>
	        </div>
	    </div>
	    <div class="footer-copyright default_color">
	        <div class="container">
	        <a class="blue-text text-lighten-3" href="http://miausoftware.mx/">2015 Miausoftware</a>
	        </div>
	    </div>
	</footer>
</body>
</html>
