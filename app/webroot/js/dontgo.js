var templates = (function(){
    var styleTemplate = function() {
        var html = '<style type="text/css">\n'+
                        '.modal-open .modal-rm {\n'+
                            'overflow-x: hidden;\n'+
                            'overflow-y: auto;\n'+
                        '}\n'+
                        '.fade.in {\n'+
                            'opacity: 1;\n'+
                        '}\n'+
                        '.modal-rm {\n'+
                            'position: fixed;\n'+
                            'top: 0;\n'+
                            'right: 0;\n'+
                            'bottom: 0;\n'+
                            'left: 0;\n'+
                            'z-index: 1050;\n'+
                            'overflow: hidden;\n'+
                            '-webkit-overflow-scrolling: touch;\n'+
                            'outline: 0;\n'+
                        '}\n'+
                        '.fade {\n'+
                            'opacity: 0;\n'+
                            '-webkit-transition: opacity .15s linear;\n'+
                            '-o-transition: opacity .15s linear;\n'+
                            'transition: opacity .15s linear;\n'+
                        '}\n'+
                        '.modal.in .modal-dialog-rm {\n'+
                            '-webkit-transform: translate(0,0);\n'+
                            '-ms-transform: translate(0,0);\n'+
                            '-o-transform: translate(0,0);\n'+
                            'transform: translate(0,0);\n'+
                        '}\n'+
                        '.modal.fade .modal-dialog-rm {\n'+
                            '-webkit-transition: -webkit-transform .3s ease-out;\n'+
                            '-o-transition: -o-transform .3s ease-out;\n'+
                            'transition: transform .3s ease-out;\n'+
                            '-webkit-transform: translate(0,-25%);\n'+
                            '-ms-transform: translate(0,-25%);\n'+
                            '-o-transform: translate(0,-25%);\n'+
                            'transform: translate(0,-25%);\n'+
                        '}\n'+
                        '.modal-dialog-rm {\n'+
                            'position: relative;\n'+
                            'width: 600px;\n'+
                            'margin: 150px auto;\n'+
                        '}\n'+
                        '.modal-content-rm {\n'+
                            'position: relative;\n'+
                            'background-color: #fff;\n'+
                            '-webkit-background-clip: padding-box;\n'+
                            'background-clip: padding-box;\n'+
                            'border: 1px solid #999;\n'+
                            'border: 1px solid rgba(0,0,0,.2);\n'+
                            'border-radius: 6px;\n'+
                            'outline: 0;\n'+
                            '-webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);\n'+
                            'box-shadow: 0 3px 9px rgba(0,0,0,.5);\n'+
                            'box-shadow: 0 5px 15px rgba(0,0,0,.5);\n'+
                        '}\n'+
                        '.modal-header {\n'+
                            'padding: 15px;\n'+
                            'border-bottom: 1px solid #e5e5e5;\n'+
                        '}\n'+
                        '.modal-body-rm {\n'+
                            'position: relative;\n'+
                            'padding: 16px;\n'+
                        '}\n'+
                        '.modal-footer-rm {\n'+
                            'padding: 15px;\n'+
                            'text-align: right;\n'+
                            'border-top: 1px solid #e5e5e5;\n'+
                        '}\n'+
                        '.modal-title-rm {\n'+
                            'margin: 0;\n'+
                            'line-height: 1.42857143;\n'+
                        '}\n'+
                        '.modal-cupon-msg {\n'+
                            'position: absolute;\n'+
                            'top: 63%;\n'+
                            'left: 0;\n'+
                            'right: 0;\n'+
                            'padding: 0 20px;\n'+
                        '}\n'+
                        '.text-center {\n'+
                            'text-align: center;\n'+
                        '}\n'+
                        '.modal-cupon-txt {\n'+
                            'line-height: 1.6;\n'+
                        '}\n'+
                        '.modal-backdrop {\n'+
                            'position: fixed;\n'+
                            'top: 0;\n'+
                            'right: 0;\n'+
                            'bottom: 0;\n'+
                            'left: 0;\n'+
                            'z-index: 1040;\n'+
                            'background-color: #000;\n'+
                            'opacity: .5;\n'+
                            'display: none;\n'+
                        '}\n'+
                        '.modal-cupon-container {\n'+
                            'position: relative;\n'+
                            'margin: -15px;\n'+
                        '}\n'+
                        '.modal-centered-rm {\n'+
                            'position: absolute;\n'+
                            'top: 50%;\n'+
                            'margin-top: -233px;\n'+
                            'left: 50%;\n'+
                            'margin-left: -300px;\n'+
                        '}\n'+
                        '.modal-cupon-container .rmkt-bg {\n'+
                            'width: 100%;\n'+
                        '}\n'+
                        '.btn {\n'+
                            'display: inline-block;\n'+
                            'padding: 6px 12px;\n'+
                            'margin-bottom: 0;\n'+
                            'font-size: 14px;\n'+
                            'font-weight: 400;\n'+
                            'line-height: 1.42857143;\n'+
                            'text-align: center;\n'+
                            'white-space: nowrap;\n'+
                            'vertical-align: middle;\n'+
                            '-ms-touch-action: manipulation;\n'+
                            'touch-action: manipulation;\n'+
                            'cursor: pointer;\n'+
                            '-webkit-user-select: none;\n'+
                            '-moz-user-select: none;\n'+
                            '-ms-user-select: none;\n'+
                            'user-select: none;\n'+
                            'background-image: none;\n'+
                            'border: 1px solid transparent;\n'+
                            'border-radius: 4px;\n'+
                        '}\n'+
                        '.btn-info {\n'+
                            'color: #fff;\n'+
                            'background-color: #5bc0de;\n'+
                            'border-color: #46b8da;\n'+
                        '}\n'+
                        '.btn-primary {\n'+
                            'color: #fff;\n'+
                            'background-color: #337ab7;\n'+
                            'border-color: #2e6da4;\n'+
                        '}\n'+
                        '.modal-cupon-msg h4{\n'+
                            'font-size: 18px;\n'+
                            'margin-top: 11px;\n'+
                            'margin-bottom: 11px;\n'+
                            'font-family: inherit;\n'+
                            'font-weight: 500;\n'+
                        '}\n'+
                    '</style>';
        return html;
    }
    var modalTemplate = function()
    {
        var html = "";
        var modal = '<div class="modal-rm fade in" id="remarketing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">\n'+
                        '<div class="modal-dialog-rm modal-centered-rm" role="document">\n'+
                            '<div class="modal-content-rm">\n'+
                                '<div class="modal-body-rm">\n'+
                                    '<div class="modal-cupon-container text-center">\n'+
                                    '<h2>THANK YOU FOR YOUR VISIT</h2>\n'+
                                        // '<img src="http://www.mexicohoteles.com.mx/images/newsletter/modal-remarketing-default-bg.jpg" width="532" height="463" class="rmkt-bg">\n'+
                                        '<div class="modal-cupon-msg text-center">\n'+
                                            '<h4 class="modal-cupon-txt">are you sure want to leave us ? Continue to browse and check who will be the great deals for you</h4>\n'+
                                            '<button id="btn-continue-rm" type="button" data-dismiss="modal" class="btn btn-primary"><span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span>Continue</button>\n'+
                                        '</div>\n'+
                                    '</div>\n'+
                                '</div>\n'+
                            '</div>\n'+
                        '</div>\n'+
                    '</div>';
        var backdrop = '<div style="display: block;" class="modal-backdrop"></div>';
        html = '<div id="modalRM" style="display:none">'+modal+backdrop+'</div>';
        return html;
    }
    return {
        modalTemplate:modalTemplate,
        styleTemplate:styleTemplate
    }
})();
var modalExit = (function(){
    var version = "0.1";
    var lenguaje = "es";
    var modal = "";
    var hideModal = function(){
        var btnClose = document.getElementById('btn-continue-rm');
        btnClose.addEventListener("click", function(event) {
            modal.style.display = 'none';
        });
    }
    var loadModalHtml = function(){
        var body = document.getElementsByTagName("BODY")[0];
        body.innerHTML = body.innerHTML+templates.modalTemplate();
        modal =  document.getElementById('modalRM');
    }
    var captureIntentExit = function(){
        window.addEventListener("beforeunload", function(event) {
            modal.style.display = 'block';
            event.returnValue = "Espera aun no te vayas..";
        });
    }
    var loader = function(){
        window.addEventListener("load", function(event) {
            loadModalHtml();
            captureIntentExit();
            hideModal();
        });
    }();
})();